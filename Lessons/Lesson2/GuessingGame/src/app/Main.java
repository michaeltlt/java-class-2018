package app;

import java.util.Random;
import java.util.Scanner;

/**
 * Игра "Угадай число"
 * Компьютер генерирует случайное число в диапазоне от 0 до 10.
 * Игрок вводит ответ с клавиатуры. Если введенное число больше или меньше загаданного,
 * компьютер дает подсказку. Если число угадано, программа завершается.
 */
public class Main {

    public static void main(String[] args) {

        // Для генерации случайных чисел используем объект класса Random.
        // Метод nextInt(максимальное_значение) выдает число типа int
        // в диапазоне о 0 до максимальное_значение, в данном случае - до 10.
        Random random = new Random();
        int number = random.nextInt(10);

        // Объект класса Scanner используется для получения данных из потока ввода.
        // В игре данные вводятся с клавиатуры, с которой связан поток System.in
        Scanner scanner = new Scanner(System.in);

        System.out.println("Я загадал число от 0 до 10. Попробуй отгадать");

        // Логическая переменная, с помощью которой будет завершен цикл while,
        // когда пользователь угадает число.
        boolean isGuessed = false;

        while(!isGuessed) {
            // Метод nextInt() класса Scanner преобразует данные, вводимые пользователем в число типа int
            int givenNumber = scanner.nextInt();

            if(givenNumber < number) {
                System.out.println("Загаданное число больше.");
            }
            else if(givenNumber > number) {
                System.out.println("Загаданное число меньше.");
            }
            else {
                System.out.println("Поздравляю, вы угадали!");
                isGuessed = true;
            }
        }
    }
}
