package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Сортировка с использованием компараторов (объектов класса Comparator).
 * При этом не нужно, чтобы класс Book реализовывал интерфейс Comparable.
 */
public class Main {

    ArrayList<Book> books = new ArrayList<>();

    public static void main(String[] args) {
        new Main().start();
    }

    public void start() {
        books.add(new Book("The great Gatsby", "Fitzgerald", 1999, 2000));
        books.add(new Book("Atlas shrugged", "Rand", 1987, 120));
        books.add(new Book("Animal farm", "Orwell", 2001, 629));
        books.add(new Book("Animal farm", "Orwell", 2001, 629));
        books.add(new Book("1984", "Orwell", 1979, 1340));

        System.out.println("Before sort ================================");
        System.out.println(books);

        TitleCompare titleCompare = new TitleCompare();
        Collections.sort(books, titleCompare);

        System.out.println("\nAfter sort by title ======================");
        System.out.println(books);

        // Еще один вариант использования компаратора:
        // здесь, вместо использования ссылки на вложенный класс, создаем анонимный класс
//        AuthorCompare authorCompare = new AuthorCompare();
        Collections.sort(books, new Comparator<Book>() {
            @Override
            public int compare(Book one, Book two) {
                return one.getAuthor().compareTo(two.getAuthor());
            }
        });


        // И еще один вариант использования компаратора:
        // здесь, вместо использования вложенного или анонимного класса, воспользуемся лямбда-выражением
        // (будет работать начиная с Java 8 и выше)
        Collections.sort(books,
                (one, two) -> one.getAuthor().compareTo(two.getAuthor()));


        System.out.println("\nAfter sort by author ======================");

        for(Book book : books) {
            System.out.println(book.getAuthor() + ": " + book.getTitle());
        }
    }

    /* Cоздаем вложенный класс, реализующий интерфейс Comparator
     * и осуществляющий сортировку по названи¤м книг. */
    class TitleCompare implements Comparator<Book> {
        @Override
        public int compare(Book one, Book two) {
            /* ѕерекладываем всю работу по сравнению на строковые объекты */
            return one.getTitle().compareTo(two.getTitle());
        }
    }

    /* Создаем вложенный класс, реализующий интерфейс Comparator
     * и осуществляющий сортировку по авторам. */
    class AuthorCompare implements Comparator<Book> {
        @Override
        public int compare(Book one, Book two) {
            /* ѕерекладываем всю работу по сравнению на строковые объекты */
            return one.getAuthor().compareTo(two.getAuthor());
        }
    }
}