package com.company;

public class ExceptionTest
{

    public static void main(String[] args)  throws Exception
    {
        new ExceptionTest().start();
    }

    private void start()  throws Exception
    {
        System.out.println("Test1: ");
        test1();
        System.out.println("Test2: " + test2());
        System.out.println("Test3: " + test3());
    }

    private void test1() throws Exception
    {
        try
        {
            throw new Exception();
        }
        catch (Exception e)
        {
            System.out.println("catch");
        }
        finally
        {
            System.out.println("finally");
        }
    }

    private int test2() throws Exception
    {
        int i = 0;
        try
        {
            throw new Exception();
        }
        catch (Exception e)
        {
            System.out.println("Catch before 10");
            i = 10;
            System.out.println("Catch after 10");
            return i;
        }
        finally
        {
            System.out.println("Finally");
            i = 20;
        }

    }

    private int test3() throws Exception
    {
        int i = 0;
        try
        {
            throw new Exception();
        }
        catch (Exception e)
        {
            i = 10;
            return i;
        } finally
        {
            i = 20;
            return i;
        }
    }
}
