package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Пример удаления элемента из коллекции во время прохода по ней.
 */
public class Main {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Integer num20 = 20;
        list.add(10);
        list.add(num20);
        list.add(30);

        for(Integer number : list) {
            list.remove(num20);     // Недопустимо. Будет сгенерировано исключение одновременного доступа к объекту.
        }


        // Вместо прохода по коллекции циклом for воспользуйтесь итератором.
        Iterator<Integer> it = list.iterator();

        while (it.hasNext()) {
            if(it.next().equals(num20)) {
                // Удаляем элемент из коллекции с помощью итератора (а не с помощью метода remove самой коллекции)
                it.remove();
            }
        }
    }
}
