package app;

public abstract class Employee {
    private String name;
    private Position position;
    private double salary;

    public Employee(String name, Position position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return name + '\t' + position + '\t' + salary;
    }
}
