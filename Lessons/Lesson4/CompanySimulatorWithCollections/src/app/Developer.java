package app;

public class Developer extends Employee {
    public Developer(String name) {
        super(name, Position.DEVELOPER);
    }

    public void program(String softName) {
        System.out.println(getName() + " разрабатывает программу " + softName);
    }
}
