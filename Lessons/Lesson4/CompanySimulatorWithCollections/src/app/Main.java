package app;

public class Main {

    public static void main(String[] args) {
        Company nanoSoftCompany = new Company(6);

        Boss manager = new Boss("Петрович");
        Developer dev1 = new Developer("Иван");
        Developer dev2 = new Developer("Коля");
        Developer dev3 = new Developer("Лысый");
        Janitor jan1 = new Janitor("Агафья Тихоновна");
        Janitor jan2 = new Janitor("Чистомен");

        nanoSoftCompany.hire(manager, 200_000);
        nanoSoftCompany.hire(dev1, 100_000);
        nanoSoftCompany.hire(dev2, 80_000);
        nanoSoftCompany.hire(dev3, 60_000);
        nanoSoftCompany.hire(jan1, 25_000);
        nanoSoftCompany.hire(jan2, 20_000);

        manager.manage();
        manager.yell();
        dev1.program("Microsoft Windows");
        dev2.program("Company Simulator");
        dev3.program("Угадай число");
        manager.yell(dev2);
        jan1.cleanUp();
        jan2.cleanUp(dev1);

        nanoSoftCompany.printSalaryReport();

//        System.out.println(nanoSoftCompany);
    }
}
