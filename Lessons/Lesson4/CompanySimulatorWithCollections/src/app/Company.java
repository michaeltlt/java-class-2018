package app;

import java.util.HashSet;
import java.util.Set;

public class Company {
    // Используем коллекцию Set, чтобы предовратить возможность добавления одного и того же Employee несколько раз
    private Set<Employee> employees;

    public Company() {
        employees = new HashSet<>();
    }

    public void hire(Employee employee, double salary) {
        if(employee != null) {
            employees.add(employee);    // Если ссылка (employee) на объект уже есть в коллекции, то новый объект в нее не добавится.
            employee.setSalary(salary);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        // Закомментированные строки делают то же, что цикл for после них,
        // но в функциональном стиле (начиная с Java 8 и выше)
//        Arrays.stream(employees).filter(employee -> employee != null)
//                .forEach(employee -> result.append(employee).append('\n'));
//
        for(Employee employee : employees) {
            if(employee != null) {
                result.append(employee).append('\n');
            }
        }

        return result.length() > 0 ? "Сотрудники:\n" + result.toString() : "Сотрудников нет";
    }

    public void printSalaryReport() {
        System.out.println("ОТЧЕТ О ЗАРПЛАТЕ СОТРУДНИКОВ");

        for(Employee employee : employees) {
            System.out.println(employee);
        }
    }
}
