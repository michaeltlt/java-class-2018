package com.company;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};

        int k = array[10];

        // Недопустимое использование исключений.
        // 1. Перехватывается исключение, вместо простой проверки на недопустимый индекс массива.
        // 2. Перехватывается "непроверяемое" исключение - избегайте таких ситуаций.
        // 3. Ошибка никак не обрабатывается в блоке catch.
        try {
            int a = array[10];
        }
        catch (ArrayIndexOutOfBoundsException ex) {

        }

        // Перехват нескольких исключений.
        // В блоке try конструктор FileReader может выбросить исключение типа FileNotFoundException,
        // а вызовы методов read и close - исключение типа IOException
        // Рекомендуется разные типы исключений обрабатывать по-разному (и в разных блоках catch, соответственно)
        try {
            FileReader fr = new FileReader("asdfasf");
            fr.read();
            fr.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Вызов метода, выбрасывающего исключения.
        // Проверяемые исключения всегда должны быть обработаны.
        try {
            read("asdfas");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Перехват исключения общего типа.
        // Так как исключение FileNotFoundException наследует от IOException, то мы можем так сделать.
        // Но в этом случае мы вынуждены обрабатывать исключения разных типов одинаково.
        try {
            read("asdfas");
        } catch (IOException e) {
            e.printStackTrace();
        }


        // Перехват исключения еще более общего типа.
        // По возможности, избегайте этого. Здесь непонятно, какие именно исключения возникают в блоке try.
        try {
            read("asdfas");
        } catch (Exception e) {
            e.printStackTrace();
        }



        // Еще один пример недопустимого использования исключений.
        // Лучше сделать простую проверку на равенство знаменателя нулю.
        try {
            int num = 10 / 0;
        }
        catch (ArithmeticException ex) {
            ex.printStackTrace();
        }



        FileReader fr = null;
        // Пример блока finally. Этот блок выполняется всегда, вне зависимости от того, было выброшено исключение
        // в блоке try или нет.
        try {
            fr = new FileReader("asdfasf");
            fr.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                fr.close();     // Метод close тоже выбрасывает "проверяемое" исключение, поэтому его надо обработать.
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        // Начиная с Java 7, некоторые классы реализуют интерфейс AutoCloseable, поэтому можно явно не вызывать
        // метод close для освобождения занятого ресурса (в данном случае - файла).
        // Однако необходимо поместить объект, у которого будет вызван close, в специальный
        // блок try(), который называется try-with-resources.
        try(FileReader reader = new FileReader("asdfasf")) {
            reader.read();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Если нужно обработать несколько разных исключений одинаковым образом, то с Java 7 мы можем
        // объявлять в блоке catch несколько исключений, объединяя их с помощью операции ИЛИ ( | )
        try(FileReader reader = new FileReader("asdfasf")) {
            reader.read();
        } catch (FileNotFoundException | IOException e) {
            e.printStackTrace();
        }

    }

    // Метод, выбрасывающй (генерирующий) исключения.
    // Если не нужно обрабатывать исключения по месту его возникновения, например в данном методе, то можно
    // пробросить исключение выше, в место вызова метода, с помощью ключевого слова throws с перечнем
    // типов исключений (через запятую, если их несколько)
    private static void read(String fileName) throws FileNotFoundException, IOException
    {
        FileReader fr = new FileReader(fileName);
        fr.read();
        fr.close();
    }
}
