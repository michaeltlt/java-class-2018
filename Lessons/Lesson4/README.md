### Проект CompanySimulatorWithCollections

*Темы*
- Замена массива на коллекцию в проекте [CompanySimulator](https://gitlab.com/michaeltlt/java-class-2018/tree/master/Lessons/Lesson3/CompanySimulator).


### Проект BookSort

*Темы*
- Сортировка коллекции типа List с помощью метода Collections.sort();
- Реализация классом интерфейса Comparable для сортировки.


### Проект ComparatorSort

*Темы*
- Сортировка коллекции типа List с помощью метода Collections.sort() и разных компараторов (реализаций интерфейса Comparator) - для сортировки по различным критериям.


### Проект RemoveElement

*Темы*
- Ошибка одновременного доступа к коллекции (ConcurrentModificationException) при переборе коллекции и удалении элемента;
- Удаление элемента из коллекции с помощью итератора.


### Проект GenericsCollectionsExample

*Темы*
- Отличия параметризованных коллекций от массивов.


### Проект Exceptions

*Темы*
- Примеры обработки исключений.


### Проект ExceptionsExample

*Темы*
- Неоднозначная ситуация - возврат из блоков catch и finally.


### Проект ExceptionHandlingBestPractices

*Темы*
- Рекомендации по обработке исключений (Best practices)

(проекты выполнены в среде Intellij IDEA)


### Дополнительная информация:

[Теория дженериков в Java или как на практике ставить скобки](https://javarush.ru/groups/posts/2004-teorija-dzhenerikov-v-java-ili-gde-na-praktike-stavitjh-skobki)