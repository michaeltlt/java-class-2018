package com.company;

import com.company.musicians.Drummer;
import com.company.musicians.Guitarist;
import com.company.musicians.Musician;

import java.util.ArrayList;

/**
 * Здесь все работает нормально. В метод viewMusicians передается
 * коллекция того типа, которая там ожидается.
 * Далее: смотрите класс CollectionExample2
 */
public class CollectionExample1 {

    ArrayList<Musician> musicians = new ArrayList<>();

    public static void main(String[] args) {
        new CollectionExample1().start();
    }

    public void start() {
        musicians.add(new Guitarist());
        musicians.add(new Drummer());
        musicians.add(new Guitarist());

        viewMusicians(musicians);
    }

    public void viewMusicians(ArrayList<Musician> musicians) {
        for(Musician musician : musicians) {
            musician.play();
        }
    }
}
