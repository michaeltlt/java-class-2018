package com.company;

import com.company.musicians.Drummer;
import com.company.musicians.Guitarist;
import com.company.musicians.Musician;

import java.util.ArrayList;

/*
 * ArrayList<Musician> разрешено передать в метод с таким же аргументом.
 * Вопрос в том, можно ли на место аргумента
 * ArrayList<Musician> подставить объект ArrayList<Guitarist>?
 * Если метод объявлен как в предыдущем примере: void viewMusicians(ArrayList<Musician> musicians),
 * то нельзя. Иначе, при добавлении объекта Drummer в коллекцию, параметризованную классом
 * Guitarist, произошла бы ошибка во время выполнения.
 */

class CollectionExample2 {

    public static void main(String[] args) {
        new CollectionExample2().start();
    }

    public void start() {
        ArrayList<Musician> musicians = new ArrayList<Musician>();

        musicians.add(new Guitarist());
        musicians.add(new Drummer());
        musicians.add(new Guitarist());

        viewMusicians(musicians);

        ArrayList<Guitarist> guitarists = new ArrayList<Guitarist>();
        guitarists.add(new Guitarist());
        guitarists.add(new Guitarist());

        System.out.println("====================================================");
        viewMusicians(guitarists);
    }

    public <T extends Musician> void viewMusicians(ArrayList<T> musicians) {

//        musicians.add(new Drummer());

        for(Musician musician : musicians) {
            musician.play();
        }
    }
}


