package com.company;

import com.company.musicians.Drummer;
import com.company.musicians.Guitarist;
import com.company.musicians.Musician;

import java.util.*;

/*
 * ArrayList<Musician> разрешено передать в метод с таким же аргументом.
 * Вопрос в том, можно ли на место аргумента
 * ArrayList<Musician> подставить объект ArrayList<Guitarist>?
 */

class CollectionExample3 {

    public static void main(String[] args) {
        new CollectionExample3().start();
    }

    public void start() {
        ArrayList<Musician> musicians = new ArrayList<>();

        musicians.add(new Guitarist());
        musicians.add(new Drummer());
        musicians.add(new Guitarist());

        viewMusicians(musicians);

        ArrayList<Guitarist> guitarists = new ArrayList<>();
        guitarists.add(new Guitarist());
        guitarists.add(new Guitarist());

        System.out.println("====================================================");
        viewMusicians(guitarists);
    }

    public void viewMusicians(ArrayList<? extends Musician> musicians) {

        for(Musician musician : musicians) {
            musician.play();
        }

        /* Если в объявлении указан заполнитель <?>,
         * компилятор не позволит выполнить добавление */
        //musicians.add(new Guitarist());
    }
}

