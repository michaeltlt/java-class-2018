package com.company;

import com.company.musicians.Drummer;
import com.company.musicians.Guitarist;
import com.company.musicians.Musician;

class ArraysExample {

    public static void main(String[] args) {
        new ArraysExample().start();
    }

    public void start() {
        Musician[] musicians = { new Guitarist(), new Drummer(), new Guitarist() };
        Guitarist[] guitarists = { new Guitarist(), new Guitarist(), new Guitarist() };

        viewMusicians(musicians);
        System.out.println("====================================================");
        viewMusicians(guitarists);
    }

    /**
     *  Позволяет принимать как Musician[], так и Guitarist[] благодаря полиморфизму.
     *  Однако, если добавить в массив musician объект Drummer, то может быть выброшено
     *  исключение во время выполнения программы.
     */
    public void viewMusicians(Musician[] musicians) {

        for(Musician musician : musicians) {
            // Можно вызывать только те методы, которые объявлены в Musician
            musician.play();
        }
    }
}

