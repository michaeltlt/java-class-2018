package com.company.musicians;

public class Guitarist extends Musician {
    public void sing() {
        System.out.println("Гитарист поет");
    }

    @Override
    public void play() {
        System.out.println("Гитарист играет");
    }
}
