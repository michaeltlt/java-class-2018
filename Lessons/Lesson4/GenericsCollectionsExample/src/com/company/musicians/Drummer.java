package com.company.musicians;

public class Drummer extends Musician {
    public void drink() {
        System.out.println("Барабанщик пьет");
    }

    @Override
    public void play() {
        System.out.println("Барабанщик играет");
    }
}