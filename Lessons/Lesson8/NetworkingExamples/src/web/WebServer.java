package web;

import java.io.FileInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

public class WebServer {

    public static void main(String[] args) {
        try {
            Properties props = new Properties();
            props.load(new FileInputStream("webserver.properties"));

            if (props.isEmpty()) {
                System.out.println("Properties don't defined.");
                System.exit(0);
            }

            String host = props.getProperty("host");
            String port = props.getProperty("port");

            ServerSocket ss = new ServerSocket(Integer.valueOf(port));

            System.out.println("Ready!");

            while(true) {
                Socket s = ss.accept();
                System.out.println("Client accepted " + s.getRemoteSocketAddress() + ":" + s.getLocalPort());
                new Thread(new SocketProc(s)).start();
            }
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
    }
}
