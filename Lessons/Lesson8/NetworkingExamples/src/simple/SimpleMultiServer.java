package simple;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleMultiServer {

    // Выбираем порт вне пределов 1-1024:
    public static final int PORT = 8080;

    public static void main(String[] args) throws IOException {
        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Started: " + s);

        try {
            while(true) {
                Socket socket = s.accept();
                new Thread(new EchoProcess(socket)).start();
            }
        } finally {
            s.close();
        }
    }

}
