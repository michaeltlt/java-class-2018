package simple;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class EchoProcess implements Runnable {

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public EchoProcess(Socket socket) throws IOException {
        this.socket = socket;

        in = new BufferedReader(new InputStreamReader(
                socket.getInputStream()));
        // Вывод автоматически выталкивается из буфера PrintWriter'ом
        out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(socket.getOutputStream())),
                true);
    }

    @Override
    public void run() {
        try {
            System.out.println("Connection accepted: " + socket);

            while (true) {
                try {
                    String str = in.readLine();

                    if (str.equals("END")) {
                        break;
                    }
                    System.out.println("Echoing: " + str);
                    out.println(str);
                }
                catch(IOException e) {
                    e.printStackTrace();
                }
            }
            // Всегда закрываем два сокета...
        } finally {
            System.out.println("closing...");
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
