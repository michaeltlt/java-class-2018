package chat;

import java.io.*;
import java.net.Socket;

public class ChatClient {

    final String HOST = "127.0.0.1";
    final int PORT = 5000;

    BufferedReader reader;
    PrintWriter writer;
    Socket sock;
    String name;

    public void go() {
        init();

        new Thread(new MessageReader(reader)).start();

        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("What is your name?");
            name = input.readLine();

            while (true) {
                String message = input.readLine();
                System.out.print("> ");
//				System.out.println(message);
                writer.println(name + ": " + message);
                writer.flush();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void init() {
        try {
            sock = new Socket(HOST, PORT);
            reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            writer = new PrintWriter(sock.getOutputStream());
            System.out.println("connection established");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ChatClient client = new ChatClient();
        client.go();
    }
}

class MessageReader implements Runnable {

    BufferedReader reader;

    public MessageReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void run() {
        String message;

        try {
            while ((message = reader.readLine()) != null) {
                System.out.println(message);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
