package chat;

import java.io.*;
import java.net.*;
import java.util.*;

public class ChatServer {

    final int PORT = 5000;
    // Коллекция ссылок на исходящие потоки клиентов
    List<PrintWriter> clientOutputStreams;

    public void go() {
        clientOutputStreams = new ArrayList<PrintWriter>();
        System.out.println("Server started.");

        try {
            ServerSocket serverSock = new ServerSocket(PORT);

            while (true) {
                Socket clientSocket = serverSock.accept();
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
                clientOutputStreams.add(writer);
                // Запустить в отдельном потоке задачу обработки сообщений от клиентов
                new Thread(new ClientHandler(clientSocket, clientOutputStreams)).start();
                System.out.println("got a connection");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ChatServer().go();
    }
}

class ClientHandler implements Runnable {

    BufferedReader reader;
    Socket sock;
    List<PrintWriter> clientOutputStreams;

    public ClientHandler(Socket clientSocket, List<PrintWriter> clientOutputStreams) {
        try {
            sock = clientSocket;
            reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            this.clientOutputStreams = clientOutputStreams;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        String message;
        System.out.println("Client started");

        try {
            while ((message = reader.readLine()) != null) {
                System.out.println(message);
                spreadMessage(message);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Рассылка сообщений всем клиентам-участникам чата
     *
     * @param message - сообщение для пересылки
     */
    private void spreadMessage(String message) {
        synchronized (clientOutputStreams) {
            for (PrintWriter writer : clientOutputStreams) {
                writer.println(message);
                writer.flush();
            }
        }
    }
}
