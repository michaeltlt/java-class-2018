package app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Оригинал: https://habr.com/post/104229/
 * При реализации пользовательских загрузчиков важно помнить следующее:
 * 1) любой загрузчик должен расширять класс java.lang.ClassLoader;
 * 2) любой загрузчик должен поддерживать модель делегирования загрузки, образуя иерархию;
 * 3) в классе java.lang.ClassLoader уже реализован метод непосредственной загрузки — defineClass(...),
 *    который байт-код преобразует в java.lang.Class, осуществляя его валидацию;
 * 4) механизм рекурентного поиска также реализован в классе java.lang.ClassLoader и заботиться об это не нужно;
 * 5) для корректной реализации загрузчика достаточно лишь переопределить метод findClass() класса java.lang.ClassLoader.
 */
public class ModuleLoader extends ClassLoader {

    /**
     * Путь до директории с модулями.
     */
    private String pathToBin;

    public ModuleLoader(String pathToBin, ClassLoader parent) {
        super(parent);
        this.pathToBin = pathToBin;
    }

    @Override
    public Class<?> findClass(String className) throws ClassNotFoundException {
        try {
            /**
             * Получем байт-код из файла и загружаем класс в рантайм
             */
            byte buffer[] = fetchClassFromFS(pathToBin + "\\" + className + ".class");
            return defineClass(className, buffer, 0, buffer.length);
        }
        catch (FileNotFoundException ex) {
            return super.findClass(className);
        }
        catch (IOException ex) {
            return super.findClass(className);
        }
    }

    /**
     * Взято из www.java-tips.org/java-se-tips/java.io/reading-a-file-into-a-byte-array.html
     */
    private byte[] fetchClassFromFS(String path) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(new File(path));

        long length = new File(path).length();

        if (length > Integer.MAX_VALUE) {
            // Файл сликом большой. Такая ситуация должна быть обработана.
        }

        byte[] buffer = new byte[(int)length];
        int offset = 0;
        int numRead = 0;

        while (offset < buffer.length
                && (numRead=is.read(buffer, offset, buffer.length-offset)) >= 0) {
            offset += numRead;
        }

        // Убедиться, что весь файл прочитан.
        if (offset < buffer.length) {
            throw new IOException("Could not completely read file " + path);
        }

        is.close();

        return buffer;
    }
}