package app;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.METHOD)
public @interface SomeAnnotation {
    /*
     * https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.6.1
     * Допустимые типы для членов аннотаций:
     *
     * примитивный тип
     * String
     * Class
     * Enum
     * другая аннотация
     * массив одного из указанных типов
     */
    double val() default 0.0;
    String str() default "...";
}
