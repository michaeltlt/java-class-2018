package app;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    @PersonDriver(gender = Gender.FEMALE, name = "Jane")
    private static Person person;


    public static void main(String args[]) {
        try {
            Method method = Main.class.getMethod("print", new Class[]{String.class});

            for (Annotation annotation : method.getAnnotations()) {
                System.out.println(annotation);
            }

            SomeAnnotation methodAnnotation = method.getAnnotation(SomeAnnotation.class);

            if (method.isAnnotationPresent(SomeAnnotation.class)) {
                System.out.println(methodAnnotation.val() + " " + methodAnnotation.str());
                print(String.valueOf(methodAnnotation.val()));
            }


            Field personField = Main.class.getDeclaredField("person");

            if(personField.isAnnotationPresent(PersonDriver.class)) {
                PersonDriver annotation = personField.getAnnotation(PersonDriver.class);

                switch (annotation.gender()) {
                    case MALE:
                        person = new Male(annotation.name());
                        break;
                    case FEMALE:
                        person = new Female(annotation.name());
                        break;
                    case UNKNOWN:
                        person = new UnknownPerson();
                }

                System.out.println("PERSON: " + person);
            }
        }
        catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }


    @SomeAnnotation (str = "Test")
    public static void print(String value) {
        System.out.println("Print method: " + value);
    }
}
