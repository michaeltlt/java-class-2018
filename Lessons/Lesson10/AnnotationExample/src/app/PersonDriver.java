package app;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PersonDriver {
//    Class<? extends Person> value();

    String name() default "Unknown";
    Gender gender() default Gender.UNKNOWN;
}
