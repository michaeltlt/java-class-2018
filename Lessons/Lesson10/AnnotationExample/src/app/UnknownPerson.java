package app;

/**
 * Класс-заглушка. Нужен для представления "пустого" объекта, вместо возврата значения null.
 */
public class UnknownPerson extends Person {
    public UnknownPerson() {
        super("Unknown", Gender.UNKNOWN);
    }
}
