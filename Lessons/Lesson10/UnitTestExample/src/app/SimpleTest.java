package app;

import org.junit.*;

public class SimpleTest {
    @BeforeClass
    // Выполняется перед тестовым классом. Должен быть public static void.
    public static void start() {
        System.out.println("Before Class");
    }

    @Before
    // Выполняется перед каждым тестовым методом. Должен быть public void.
    public void beforeSingle() {
        System.out.println("Before test method");
    }

    @Test
    // Один из тестовых методов. Должен быть public void.
    public void test1() {
        System.out.println("Test #1 Passed");
    }

    @Test(expected = NullPointerException.class)
    //Тест будет пройден, даже если возникнет NullPointerException.
    public void test2() {
        System.out.println("Test #2 Passed");
        throw new NullPointerException();
    }

    @Test(timeout = 5000)
    //Если выполненные теста превысит 5000 мс.
    public void test4() {
        while (true) ;
    }

    @After
    // Выполняется после каждого тестового метода. Должен быть public void.
    public void afterSingle() {
        System.out.println("After test method");
    }

    @AfterClass
    // Выполняется после тестового класса. Должен быть public static void.
    public static void complete() {
        System.out.println("After class. SimpleTest completed");
    }
}
