package app;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
    private Calculator calculator;

    @Before
    public void init() {
        calculator = new Calculator();
    }

    @Test
    public void evaluatesExpression() {
//        assert(4 < 3);
        int sum = calculator.evaluate("1+2+3");
        assertEquals(6, sum);
    }
}