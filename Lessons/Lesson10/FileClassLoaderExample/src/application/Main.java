package application;

import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {
        String classFileName = "Person.class"; //args[0];

        ClassLoader loader = new FileClassLoader(classFileName);

        try {
            Object person = loader.loadClass("Person").newInstance();
            Method setName = person.getClass().getDeclaredMethod("setName", String.class);
            Method getName = person.getClass().getDeclaredMethod("getName", (Class<?>[]) null);
            setName.setAccessible(true);
            setName.invoke(person, "Alex");
            System.out.println(getName.invoke(person));
            System.out.println(person.getClass().getClassLoader().toString());
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
