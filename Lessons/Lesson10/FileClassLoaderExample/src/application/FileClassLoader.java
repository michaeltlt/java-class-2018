package application;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileClassLoader extends ClassLoader {
    private String fileName;

    public FileClassLoader(String fileName) {
//        super(parent);
        this.fileName = fileName;
    }

    @Override
    public Class<?> findClass(String className) {
        //byte[] b = loadClassData(className + ".class");
        byte[] b = loadClassData(fileName);
        return defineClass(className, b, 0, b.length);
    }

    private byte[] loadClassData(String fileName) {
        try {
            FileInputStream fis = new FileInputStream(fileName);
            InputStream bis = new BufferedInputStream(fis);
            byte[] buf = new byte[bis.available()];
            bis.read(buf);

            return buf;
        }
        catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
