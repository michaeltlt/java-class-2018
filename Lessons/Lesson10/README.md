### Ссылки

#### Class Loaders
- [Загрузка классов в Java. Теория](https://habr.com/post/103830/)
- [Загрузка классов в Java. Практика](https://habr.com/post/104229/)
- [Java – Class Loading](https://dnhome.wordpress.com/2012/06/26/java-class-loading/)

#### Аннотации
- [Как написать свою Аннотацию в Java?](https://devcolibri.com/%D0%BA%D0%B0%D0%BA-%D0%BD%D0%B0%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D1%8C-%D1%81%D0%B2%D0%BE%D1%8E-%D0%B0%D0%BD%D0%BD%D0%BE%D1%82%D0%B0%D1%86%D0%B8%D1%8E-%D0%B2-java/)

#### TDD и модульные тесты
- [Об использовании модульных тестов и TDD](https://eax.me/unit-testing/)
- [Программирование-по-Контракту в Java](https://habr.com/company/golovachcourses/blog/222679/)