package app;

public class Person {
    private String name;
    private int age;

    public Person() {
        name = "Unknown";
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "name: " + name + ", age: " + age;
    }

    public void rename(String newName) {
        name = newName;
    }

    private void whoAmI() {
        System.out.println("I am a Person.");
    }
}
