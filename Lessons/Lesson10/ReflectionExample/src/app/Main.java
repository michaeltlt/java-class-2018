package app;

import java.lang.reflect.*;

public class Main {

    public static void main(String[] args) {
        try {
            Class<Person> personClass = Person.class;   // Получаем ссылку на класс
            Person unknown = personClass.newInstance();     // Создаем объект класса Person
            System.out.println(unknown);
//            System.out.println(personClass.getField("name"));

            Class personClass2 = Class.forName("app.Person");   // Еще один способ получения ссылки на класс
            Class paramTypes[] = {String.class, int.class};
            Constructor<Person> constructor = personClass2.getConstructor(paramTypes);  // Ищем конструктор с параметрами указанных типов
            Object[] params = {"Vasya", 30};
            Person vasya = constructor.newInstance(params);

            System.out.println(vasya);

            Method renameMethod = personClass.getMethod("rename", new Class[]{String.class});
            renameMethod.invoke(unknown, "Petya");
            System.out.println("Renamed person: " + unknown);

            // Используем getDeclaredField вместо getField, чтобы найти приватное поле.
            Field ageField = personClass2.getDeclaredField("age");
            // Чтобы получить доступ к приватному члену класса, нужно вызвать метод
            // setAccessible с параметром true.
            ageField.setAccessible(true);
            ageField.setInt(vasya, 22);
            System.out.println(vasya);

            // Вызываем приватный метод.
            Method hiddenMethod = personClass2.getDeclaredMethod("whoAmI", new Class[]{});
            hiddenMethod.setAccessible(true);
            hiddenMethod.invoke(vasya, new Object[]{});

//            Person p = new Person();
//            p.whoAmI(); // Нет доступа к методу

            // Получаем модификаторы доступа метода hiddenMethod
            int hiddenMethodModifiers = hiddenMethod.getModifiers();
            System.out.println("Is method hiddenMethod private? " + Modifier.isPrivate(hiddenMethodModifiers));

            // Работа с массивами
            double[] position = { 10.0, 300.4 };
            double xPos = (double)Array.get(position, 0);
            double yPos = Array.getDouble(position, 1);
            System.out.println("xPos = " + xPos + ", yPos = " + yPos);

        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
