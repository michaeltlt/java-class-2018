package app;

import java.util.concurrent.*;

/**
 * Пример игнорирования секции finally демонами при завершении
 * работы программы до окончания работы потоков-демонов.
 */
public class DaemonsFinallyExample implements Runnable {

    private int id;
    private int timeout;

    public DaemonsFinallyExample(int id, int timeout) {
        this.id = id;
        this.timeout = timeout;
    }

    @Override
    public void run()
    {
        System.out.println(id + ": start " + (Thread.currentThread().isDaemon() ? "daemon" : "regular thread"));

        try {
            TimeUnit.SECONDS.sleep(timeout);
        }
        catch (InterruptedException ex) {
            System.out.println("Daemon interrupted");
        }
        finally {
            System.out.println(id + " finally section");
        }
    }

    // Если установить демонам таймаут больше, чем у обычного потока, то можно увидеть, что секции finally
    // у демонов не выполняются при завершении работы программы (основного потока).
    // Если сделать таймауты у всех потоков одинаковыми, то демоны успевают завершиться и зайти в секцию finally.
    public static void main(String[] args)
    {
        for (int i = 0; i < 3; i++) {
            Thread daemon = new Thread(new DaemonsFinallyExample(i, 10));
            // Закомментировать вызов setDaemon и проверить выполнение finally
            daemon.setDaemon(true);	// Необходимо вызвать перед start()
            daemon.start();
        }

        // Обычный поток (не демон)
        Thread thread = new Thread(new DaemonsFinallyExample(10, 1));
        thread.start();

        System.out.println("All daemons are running");
    }
}
