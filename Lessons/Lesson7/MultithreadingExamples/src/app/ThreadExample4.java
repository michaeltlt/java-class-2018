package app;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


/**
 * Пример получения результата из задачи, запущенной в отдельном потоке.
 */
public class ThreadExample4 {

    public static void main(String[] args)
    {
        ExecutorService exec = Executors.newCachedThreadPool();

        ArrayList<Future<String>> results = new ArrayList<Future<String>>();

        for (int i = 0; i < 5; i++) {
            results.add(exec.submit(new TaskWithResult(i)));
        }
        
        try {
//            Thread.sleep(5000);
            TimeUnit.SECONDS.sleep(5);
        } 
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        for (Future<String> fs : results) {
            try {
                /* Чтобы узнать, завершена ли операция, можно
                 * обратиться к Future c запросом isDone().
                 * Вызов get() блокируется до завершения. */
                if(fs.isDone()) {
                    System.out.println("Done");
                    System.out.println(fs.get());
                }
            }
            catch (InterruptedException ex) {
                System.out.println(ex);
                return;
            }
            catch (ExecutionException ex) {
                System.out.println(ex);
            }
            finally {
                exec.shutdown();
            }
        }
    }

}

class TaskWithResult implements Callable<String> {

    private int id;

    public TaskWithResult(int id)
    {
        this.id = id;
    }

    @Override
    public String call()
    {
        return "result of TaskWithResult " + id;
    }
}
