package app;

/* Lock используется при решении особых задач.
 * Например для получения блокировки с неудачным исходом
 * или в течение некоторого промежутка времени
 * с последующим отказом. */

import java.util.concurrent.*;
import java.util.concurrent.locks.*;


public class AttemptLocking {
    /* Класс ReentrantLock делает возможной попытку
     * установления блокировки с последующим
     * отказом от нее. */
    private ReentrantLock lock = new ReentrantLock();

    public void untimed() {
        boolean captured = false;

        try {
            captured = lock.tryLock();
            System.out.println("tryLock(): " + captured);
        } finally {
            if(captured) lock.unlock();
        }
    }

    /* Делается попытка установления блокировки,
     * которая может завершиться неудачей через 2 сек. */
    public void timed() {
        boolean captured = false;

        try {
            captured = lock.tryLock(2, TimeUnit.SECONDS);
            System.out.println("tryLock(2, TimeUnit.SECONDS): " + captured);
        }
        catch(InterruptedException ex) {
            throw new RuntimeException(ex);
        } finally {
            if(captured) lock.unlock();
        }
    }


    public static void main(String[] args) {
        final AttemptLocking al = new AttemptLocking();
        al.untimed();			// true - блокировка доступна
        al.timed();				// true - блокировка доступна

        // Создаем отдельную задачу для установления блокировки
        new Thread() {
            { setDaemon(true); }

            @Override
            public void run() {
                al.lock.lock();
                System.out.println("acquired");
            }
        }.start();

        Thread.yield();
        al.untimed();			// false - блокировка захвачена задачей
        al.timed();				// false - блокировка захвачена задачей
    }
}