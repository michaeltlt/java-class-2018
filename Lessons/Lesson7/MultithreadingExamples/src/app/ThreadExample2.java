package app;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadExample2 {

    public static void main(String[] args)
    {
        // Создаем контекст для выполнения объектов Runnable
        ExecutorService exec = Executors.newCachedThreadPool();
//        ExecutorService exec = Executors.newFixedThreadPool(5);

        /* Если SingleThreadExecutor передается более одной задачи,
         * то все они ставятся в очередь, и каждая из них 
         * отрабатывает до завершения перед началом следующей задачи, 
         * причем все они используют один и тот же поток. */
//        ExecutorService exec = Executors.newSingleThreadExecutor();

        runCountDown(exec);

        // Запретить передачу новых задач Executor-у.
        // Программа продолжает выполнение до завершения
        // всех потоков.
        exec.shutdown();
    }

    private static void runCountDown(ExecutorService exec)
    {
        for (int i = 0; i < 5; i++) {
            exec.execute(new CountDown());
        }
    }

}
