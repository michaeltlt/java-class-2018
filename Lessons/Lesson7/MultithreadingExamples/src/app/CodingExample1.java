package app;

/**
 * Вариант создания потока - наследование класса Thread.
 */
public class CodingExample1 extends Thread {

    private int countDown = 5;
    private static int threadCount = 0;

    public CodingExample1()
    {
        // Задаем имя потока вызовом конструктора суперкласса
        super(Integer.toString(++threadCount));
        start();
    }

    public String toString()
    {
        return "#" + getName() + "(" + countDown + ") ";
    }

    @Override
    public void run()
    {
        while (true) {
            System.out.println(this);

            if (--countDown == 0) {
                return;
            }
        }
    }

    public static void main(String[] args)
    {
        for (int i = 0; i < 3; i++) {
            new CodingExample1();
        }
    }
}
