package app;

// Автоматическое выделение собственной памяти каждому потоку.

import java.util.concurrent.*;
import java.util.*;

class Accessor implements Runnable {
    private final int id;

    public Accessor(int idn) { id = idn; }

    @Override
    public void run() {
        //while(!Thread.currentThread().isInterrupted()) {
        for (int i = 0; i < Math.random() * 10; i++) {
            ThreadLocalVariableHolder.increment();
            System.out.println(this);
            Thread.yield();
        }

        System.out.println("#" + id + " stopped.");
    }

    public String toString() {
        return "#" + id + " " + ThreadLocalVariableHolder.get();
    }
}


public class ThreadLocalVariableHolder {
    private static ThreadLocal<Integer> value =
            new ThreadLocal<Integer>() {
                private Random rand = new Random(47);

                /* Возвращает начальное значение данной
                 * ThreadLocal-переменной для текущего потока.
                 * Вызывается, когда поток впервые вызывает метод get().
                 *
                 * Если здесь выполняется логика, связанная с изменением
                 * других объектов, то этот метод нужно синхронизировать. */
                @Override
                protected synchronized Integer initialValue() {
                    return rand.nextInt(10000);
                }
            };

    public static void increment() {
        value.set(value.get() + 1);
    }

    public static int get() { return value.get(); }

    public static void main(String args[]) {
        ExecutorService exec = Executors.newCachedThreadPool();

        for(int i = 0; i < 2; i++) {
            exec.execute(new Accessor(i));
        }

		/*try {
			TimeUnit.SECONDS.sleep(3);
		}
		catch(InterruptedException ex) {
			System.out.println(ex);
		}*/

        exec.shutdown();
    }
}