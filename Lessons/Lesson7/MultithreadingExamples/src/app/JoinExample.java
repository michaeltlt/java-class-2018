package app;

/**
 * Пример ожидания потоком другого потока.
 * Каждый поток присоединяется к другому потоку с помощью метода join и ожидает его завершения.
 */
public class JoinExample {

    public static void main(String[] args)
    {
        MyThread t1 = new MyThread("1", 1000, null);
        MyThread t2 = new MyThread("2", 4000, t1);
        MyThread t3 = new MyThread("3", 600, t2);
        MyThread t4 = new MyThread("4", 500, t3);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}

class MyThread extends Thread {

    private String name;
    private int sleepTime;
    private MyThread waitsFor;

    /**
     *
     * @param name Имя потока
     * @param sleepTime Время, на которое поток заснет (имитация деятельности)
     * @param waitsFor Ссылка на поток, к которому текущий поток присоединиться (кого он будет ждать).
     */
    MyThread(String name, int sleepTime, MyThread waitsFor)
    {
        this.name = name;
        this.sleepTime = sleepTime;
        this.waitsFor = waitsFor;
    }

    public void run()
    {
        System.out.println("Start " + name + " ");

        try {
            Thread.sleep(sleepTime);
        }
        catch (InterruptedException ie) {
        }

        System.out.println(name + "? ");

        if (waitsFor != null) {
            try {
                waitsFor.join();
                System.out.println(name + " waits for " + waitsFor.getThreadName());
            }
            catch (InterruptedException ie) {
                System.out.println(name + " interrupted ");
            }
        }

        System.out.println("Stop " + name);
    }

    public String getThreadName() {
        return name;
    }
}
