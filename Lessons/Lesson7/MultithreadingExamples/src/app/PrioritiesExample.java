package app;

import java.util.concurrent.*;

/**
 * Пример установки значений приоритетов потокам.
 */
public class PrioritiesExample implements Runnable {

    private int countDown = 5;
    private int priority;
    private int id;
    private volatile double d = 0;

    public PrioritiesExample(int priority, int id)
    {
        this.priority = priority;
        this.id = id;
    }

    public String toString()
    {
        return id + ": " + countDown;
    }

    @Override
    public void run()
    {
        Thread.currentThread().setPriority(priority);   // Установку приоритета нужно делать в методе run. Почему не в конструкторе?

        // Много вычислений
        while (true) {
            for (int i = 1; i < 100000; i++) {
                d += (Math.PI + Math.E) / (double) i;

                if (i % 1000 == 0) {
                    Thread.yield();
                }
            }

            System.out.println(this);

            if (--countDown == 0) {
                return;
            }
        }
    }

    public static void main(String[] args)
    {
        // Создаем контекст для выполнения объектов Runnable
        ExecutorService exec = Executors.newCachedThreadPool();

        for (int i = 0; i < 5; i++) {
            exec.execute(new PrioritiesExample(Thread.MIN_PRIORITY, i));
        }

        exec.execute(new PrioritiesExample(Thread.MAX_PRIORITY, 10));

	// Запретить передачу новых задач Executor-у.
        // Программа продолжает выполнение до завершения
        // всех потоков.
        exec.shutdown();
    }
}
