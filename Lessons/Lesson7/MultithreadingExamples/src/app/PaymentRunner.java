package app;

import java.util.Scanner;

class Payment {
	private int amount;
	private boolean close;

	public int getAmount() {
		return amount;
	}

	public boolean isClose() {
		return close;
	}

	public synchronized void doPayment() {
		try {
			System.out.println("Start payment: ");

			while(amount <= 0) {
				// Остановка выполнения и освобождение блокировки. 
				// После возврата блокировки выполнение будет продолжено.
				wait();
			}

			close = true;
		}
		catch(InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Payment is closed: " + close);
	}

	public void initAmount() {
		Scanner scan = new Scanner(System.in);
		amount = scan.nextInt();
	}
}

public class PaymentRunner {
	public static void main(String[] args) throws InterruptedException {
		final Payment payment = new Payment();

		new Thread() {
			@Override
			public void run() {
				payment.doPayment();
			}
		}.start();

		Thread.sleep(200);

		synchronized(payment) {
			System.out.println("Init amount: ");
			payment.initAmount();
			// payment.notify();
		}

		synchronized(payment) {
			payment.wait(1000);
			System.out.println("ok");
		}
	}
}