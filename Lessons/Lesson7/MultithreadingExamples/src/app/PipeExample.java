package app;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Пример обмена данными между потоками посредством каналов (потоков данных).
 */
public class PipeExample {
    public static void main(String[] args) throws Exception {

        // Задачи Sender и Receiver должны быть полностью сконструированы
        // перед запуском в потоках. Иначе возможно несогласованное поведение.
        Sender sender = new Sender();
        Receiver receiver = new Receiver(sender);

        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(sender);
        exec.execute(receiver);

        TimeUnit.SECONDS.sleep(4);

        exec.shutdownNow();
    }
}


class Sender implements Runnable {
    private Random random = new Random(47);
    private PipedWriter writer = new PipedWriter();

    @Override
    public void run() {
        try {
            while (true) {
                for(char c = 'A'; c <= 'z'; c++) {
                    writer.write(c);
                    TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                }
            }
        } catch (IOException e) {
            System.out.println(e + " Sender write exception.");
        } catch (InterruptedException e) {
            System.out.println(e + " Sender sleep interrupted.");
        }
    }

    public PipedWriter getWriter() {
        return writer;
    }
}


class Receiver implements Runnable {
    private PipedReader reader;

    public Receiver(Sender sender) throws IOException {
        reader = new PipedReader(sender.getWriter());
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("Read " + (char)reader.read() + ". ");
            }
        } catch (IOException e) {
            System.out.println(e + " Receiver read exception.");
        }
    }
}
