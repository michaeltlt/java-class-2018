package app;

import java.util.concurrent.TimeUnit;

/**
 * Пример потоков-демонов
 */
public class DaemonsExample implements Runnable {

    private int id;

    public DaemonsExample(int id) {
        this.id = id;
    }

    @Override
    public void run()
    {
        try {
            while (true) {
                TimeUnit.MILLISECONDS.sleep(1);
                System.out.println(Thread.currentThread() + " " + this);
            }
        } catch (InterruptedException ex) {
            System.out.println("sleep() прерван");
        }
    }

    @Override
    public String toString() {
        return "Демон " + id;
    }

    public static void main(String[] args)
    {
        for (int i = 0; i < 5; i++) {
            Thread daemon = new Thread(new DaemonsExample(i));
            daemon.setDaemon(true);	// Необходимо вызвать перед start()
            daemon.start();
        }

        System.out.println("Все демоны запущены");

        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException ex) {
            System.out.println(ex);
        }
    }
}
