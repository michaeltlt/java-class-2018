package app;

public class CountDown implements Runnable {

    private int countDown = 10;         // Счетчик для обратного отсчета
    private static int taskCount = 0;
    private final int id = taskCount++;	// Номер задачи

    public CountDown()
    {
        
    }
    
    public CountDown(int countDown)
    {
        this.countDown = countDown;
    }

    public String status()
    {
        return "#" + id + "("
                + (countDown > 0 ? countDown : "Exit!")
                + ") ";
    }

    @Override
    public void run()
    {
        while (countDown-- > 0) {
            System.out.print(status());
            /* Очередная часть выполнена, теперь можно
             * на время переключиться на другую задачу */
            Thread.yield();
        }
    }
}