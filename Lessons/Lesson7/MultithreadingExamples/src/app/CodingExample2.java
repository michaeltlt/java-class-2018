package app;

/**
 * Вариант создания потока - самоуправляемая реализация Runnable.
 */
public class CodingExample2 implements Runnable {

    private int countDown = 5;
    private Thread t = new Thread(this);

    /* Запуск потоков в конструкторе может создать проблемы.
     * Например, до завершения конструктора может быть запущена
     * другая задача, которая обратится к объекту в нестабильном
     * состоянии. */
    public CodingExample2()
    {
        t.start();
    }

    public String toString()
    {
        return Thread.currentThread().getName() + "(" + countDown + ") ";
    }

    @Override
    public void run()
    {
        while (true) {
            System.out.println(this);

            if (--countDown == 0) {
                return;
            }
        }
    }

    public static void main(String[] args)
    {
        for (int i = 0; i < 3; i++) {
            new CodingExample2();
        }
    }
}
