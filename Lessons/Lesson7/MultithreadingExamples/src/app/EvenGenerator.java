package app;

import java.util.concurrent.*;


// Единственной задачей класса является проверка четности
class EvenChecker implements Runnable {

    private EvenGenerator generator;
    private final int id;

    public EvenChecker(EvenGenerator g, int id)
    {
        generator = g;
        this.id = id;
    }

    /* Все задачи EvenChecker проверяют, не были ли они отменены.
     * При таком подходе задачи, использующие общий ресурс, наблюдают
     * за ним, ожидая сигнала завершения. Тем самым устраняется 
     * "ситуация гонки", когда две и более задачи торопятся отреагировать
     * на некоторое условие, что приводит к возникновению конфликтов 
     * или получению некорректных результатов. */
    @Override
    public void run()
    {
        while (!generator.isCancelled()) {
            /* Одна задача может вызвать next() после того, как другая
             * задача выполнит первый инкремент, но до второго инкремента.
             * При этом значение оказывается в “некорректном” состоянии. */
            int val = generator.next();

            // Если число нечетное, выводится сообщение 
            // и программа завершается.
            if (val % 2 != 0) {
                System.out.println(val + " нечетное. Поток " + id);
                generator.cancel();	// Отмена всех EvenChecker
            }
        }
    }

    public static void test(EvenGenerator gp)
    {
        System.out.println("Для выхода из программы нажмите Ctrl+C");

        ExecutorService exec = Executors.newCachedThreadPool();

        for (int i = 0; i < 5; i++) {
            exec.execute(new EvenChecker(gp, i));
        }

        exec.shutdown();
    }
}

public class EvenGenerator {

    /* Посколько флаг cancelled относится к типу boolean, 
     * простые операции вроде присваивания и возврата 
     * выполняются атомарно, без возможности прерывания. */
    private volatile boolean cancelled = false;
    
    private int currentEvenValue = 0;
    
    public void cancel()
    {
        cancelled = true;
    }	// Отмена

    public boolean isCancelled()
    {
        return cancelled;
    }

    // Получение следующего значения
    /*
     Следует синхронизировать данный метод. Тогда первая задача, входящая в метод,
     установит блокировку, а все остальные задачи, пытающиеся ее установить,
     блокируются до момента ее снятия первой задачей.
     Таким образом, в любой момент времени только одна задача может проходить по
     коду, защищенному мьютексом.
     Уберите ключевое слово synchronized и проверьте результат работы программы.
    */
    public synchronized int next()
    {
        /* Операция инкремента состоит из нескольких инструкций и
         * может быть прервана планировщиком. */
        ++currentEvenValue;	// Опасная точка!
        ++currentEvenValue;

        return currentEvenValue;
    }

    public static void main(String[] args)
    {
        EvenChecker.test(new EvenGenerator());
    }
}
