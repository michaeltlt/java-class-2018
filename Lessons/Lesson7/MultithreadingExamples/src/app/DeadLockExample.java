package app;

public class DeadLockExample {

    public static void main(String[] args)
    {
        ThreadA threadA = new ThreadA();
        ThreadB threadB = new ThreadB();

        threadA.setThreadB(threadB);
        threadB.setThreadA(threadA);
        threadA.start();
        threadB.start();
    }
}

class ThreadA extends Thread {

    Thread t2;

    public ThreadA()
    {
        System.out.println("ThreadA создан");

    }

    @Override
    public void run()
    {
        System.out.println("ThreadA запущен");
        try {
            sleep(1000);
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            System.out.println("ThreadA ждет, когда завершится ThreadB");
            t2.join();

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("ThreadA завершен");
    }

    public void setThreadB(Thread t)
    {
        this.t2 = t;
    }
}

class ThreadB extends Thread {

    Thread t1;

    public ThreadB()
    {
        System.out.println("ThreadB создан");
    }

    @Override
    public void run()
    {
        System.out.println("ThreadB запущен");

        try {
            System.out.println("ThreadB ждет, когда завершится ThreadA");
            t1.join();

        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ThreadB завершен");
    }

    public void setThreadA(Thread t)
    {
        this.t1 = t;
    }
}
