package app;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * Пример периодического процесса
 */
public class ThreadExample3 {

    public static void main(String[] args)
    {
        ScheduledExecutorService scheduler
                = Executors.newSingleThreadScheduledExecutor();

        scheduler.scheduleWithFixedDelay(
                new Task(), 0, 2, TimeUnit.SECONDS);

        try 
        {
//             Старый стиль:
//             Thread.sleep(10000);
//             Стиль Java SE 5:
            TimeUnit.SECONDS.sleep(10);
            scheduler.shutdown();
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}

class Task implements Runnable {

    @Override
    public void run()
    {
        System.out.println("start ... stop");
        System.out.println("--------------");
    }
}
