package app.philosophers;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Демонстрация скрытой возможности взаимной блокировки.
 */
public class DeadlockDining {
    public static void main(String[] args) throws Exception {
        int ponder = 5;     // Уменьшение значения времени на раздумья приводит к взаимной блокировке
        int size = 5;

        ExecutorService exec = Executors.newCachedThreadPool();
        Chopstick[] sticks = new Chopstick[size];

        for (int i = 0; i < size; i++) {
            sticks[i] = new Chopstick();
        }

        for (int i = 0; i < size; i++) {
            exec.execute(new Philosopher(sticks[i], sticks[(i + 1) % size], i, ponder));
        }

        TimeUnit.SECONDS.sleep(5);

        exec.shutdown();
    }
}
