package app.philosophers;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Philosopher implements Runnable {
    private Chopstick left;
    private Chopstick right;
    private Random random = new Random(47);

    private final int id;
    private final int ponderFactor; // Фактор обдумывания. Используется для вычисления времени на обдумывание.

    public Philosopher(Chopstick left, Chopstick right, int id, int ponderFactor) {
        this.left = left;
        this.right = right;
        this.id = id;
        this.ponderFactor = ponderFactor;
    }

    private void pause() throws InterruptedException {
        if(ponderFactor == 0) return;

        TimeUnit.MILLISECONDS.sleep(random.nextInt(ponderFactor * 250));
    }

    @Override
    public void run() {
        try {
            while(!Thread.interrupted()) {
                System.out.println(this + " думает.");
                pause();

                // Философ проголодался
                System.out.println(this + " берет правую.");
                right.take();

                System.out.println(this + " берет левую.");
                left.take();

                System.out.println(this + " ест.");
                pause();

                right.drop();
                left.drop();
            }
        } catch (InterruptedException e) {
            System.out.println(this + " выход через прерывание.");
        }
    }

    @Override
    public String toString() {
        return "Философ "+ id;
    }
}
