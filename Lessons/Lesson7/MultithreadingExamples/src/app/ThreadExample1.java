package app;

public class ThreadExample1 {

    public static void main(String[] args) {
        CountDown launch = new CountDown();
//        launch.run();     // Неправильно. Для запуска потока используется метод Thread.start();
        Thread thread = new Thread(launch);
        thread.start();

        /* После вызова start() управление немедленно
         * передается в основную программу. */
        // 2 вариант: дополнительные потоки
        for (int i = 0; i < 5; i++) {
            new Thread(new CountDown()).start();
        }

        System.out.println("Waiting for CountDown");
    }

}
