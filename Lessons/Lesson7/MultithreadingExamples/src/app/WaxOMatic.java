package app;

import java.util.concurrent.*;

class Car {
    // Текущее состояние процесса полировки.
    private boolean waxOn = false;

    // Покрыто воском
    public synchronized void waxed()
    {
        waxOn = true;   // Готово к обработке
        // Задача, приостановленная вызовом wait(), активизируется.
        notifyAll();
        
        System.out.println("Готово к полировке.");
    }

    // Отполировано
    public synchronized void buffed()
    {
        waxOn = false;  // Готово к нанесению следующего слоя воска
        // Задача, приостановленная вызовом wait(), активизируется.
        notifyAll();
        
        System.out.println("Отполировано.");
    }

    public synchronized void waitForWaxing() throws InterruptedException
    {
        System.out.println("Ожидание нанесения пасты.");
        // Вызывающий поток приостанавливается, а блокировка снимается.
        while (waxOn == false) {
            wait();
        }
    }

    public synchronized void waitForBuffing() throws InterruptedException
    {
        System.out.println("Ожидание полировки.");
        // Вызывающий поток приостанавливается, а блокировка снимается.
        while (waxOn == true) {
            wait();
        }
    }
}

class WaxOn implements Runnable {

    private final Car car;

    public WaxOn(Car car)
    {
        this.car = car;
    }

    @Override
    public void run()
    {
        try {
            while (!Thread.interrupted()) {
                System.out.println("Нанести пасту!");
                TimeUnit.MILLISECONDS.sleep(200);
                car.waxed();
                car.waitForBuffing();
            }
        } catch (InterruptedException ex) {
            System.out.println("Exiting via interrupt");
        }

        System.out.println("Задача WaxOn завершена.");
    }
}

class WaxOff implements Runnable {

    private final Car car;

    public WaxOff(Car car)
    {
        this.car = car;
    }

    @Override
    public void run()
    {
        try {
            while (!Thread.interrupted()) {
                car.waitForWaxing();
                System.out.println("Паста нанесена. Полируем.");
                TimeUnit.MILLISECONDS.sleep(200);
                car.buffed();
            }
        } catch (InterruptedException ex) {
            System.out.println("Exiting via interrupt");
        }

        System.out.println("Задача WaxOff завершена.");
    }
}

public class WaxOMatic {

    public static void main(String[] args) throws Exception
    {
        Car car = new Car();
        ExecutorService exec = Executors.newCachedThreadPool();
        
        exec.execute(new WaxOff(car));
        exec.execute(new WaxOn(car));
        TimeUnit.SECONDS.sleep(2);
        
        exec.shutdownNow();
    }
}
