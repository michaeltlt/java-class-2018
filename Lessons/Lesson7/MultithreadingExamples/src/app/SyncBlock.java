package app;

/* Пример критической секции.
 * Один из потоков блокирует объект, и до тех пор, пока он не закончит
 * выполнение блока синхронизации, в котором производится изменение значения
 * объекта, ни один другой поток не может вызвать синхронизированный блок для этого объекта.
 *
 * Если в коде убрать синхронизацию объекта s, то вывод будет другим, так как другой
 * поток сможет получить доступ к объекту и изменить его раньше, чем первый закончит
 * выполнение цикла. */
public class SyncBlock {

    public static void main(String args[])
    {
        final StringBuffer s = new StringBuffer();

        new Thread() {
            @Override
            public void run()
            {
                int i = 0;

                /*
                 Такая конструкция называется синхронизированной блокировкой.
                 Перед входом в нее необходимо получить блокировку для
                 объекта s. Если блокировка уже предоставлена другому потоку,
                 вход в данных фрагмент кода запрещается до тех пор, пока
                 блокировка не будет снята.
                */
                synchronized (s) {
                    while (i++ < 3) {
                        s.append("A");

                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            System.err.print(e);
                        }

                        System.out.println(s);
                    }
                } //конец synchronized
            }
        }.start();

        new Thread() {
            @Override
            public void run()
            {
                int j = 0;

                synchronized (s) {
                    while (j++ < 3) {
                        s.append("B");
                        System.out.println(s);

                    }
                } //конец synchronized
            }
        }.start();
    }
}
