package app;

import java.util.concurrent.*;

// Программа находит нечетные значения и завершается.
public class AtomicityExample implements Runnable {

    // Отсутствие volatile приведет к проблемам с видимостью.
    private int i = 0;

    /* Хотя и return i является атомарной операцией, 
     * отсутствие синхронизации позволит читать значение 
     * объекта, когда он находится в нестабильном состоянии.
     * Уберите synchronized и проверьте работу программы.*/
    public synchronized int getValue()
    {
        return i;
    }

    public synchronized void evenIncrement()
    {
        i++;    // Инкремент не является атомарной операцией в Java!
        i++;
    }

    @Override
    public void run()
    {
        while (true) {
            evenIncrement();
        }
    }

    public static void main(String[] args)
    {
        ExecutorService exec = Executors.newCachedThreadPool();
        AtomicityExample at = new AtomicityExample();
        exec.execute(at);

        while (true) {
            int val = at.getValue();

            if (val % 2 != 0) {
                System.out.println(val);
                System.exit(0);
            }
        }
    }
}
