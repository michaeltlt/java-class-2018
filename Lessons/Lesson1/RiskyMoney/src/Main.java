/**
 * Пример недопустимости использования данных типа double или float
 * в финансовых расчетах.
 */
public class Main {

    public static void main(String[] args) {

        double dollars = 1.0;   // Сколько долларов в кармане
        int candies = 0;        // Количество купленных конфет

        for (double price = 0.1; dollars >= price ; price += 0.1) {
            dollars -= price;
            candies++;
        }

        System.out.println("Куплено конфет: " + candies);
        System.out.println("Осталось на счете: " + dollars);

        // Из-за особенностей представления чисел с плавающей точкой
        // получается неточный ответ.
        // Решение: для финансовых данных использовать класс BigDecimal
        // или типы int и long (расчет вести не в долларах, а в центах).
    }
}
