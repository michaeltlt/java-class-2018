package com.company;

import java.io.*;

/**
 * Пример "традиционного" чтения файла и использование
 * конструкции try-with-resources.
 */
public class Main {

    public static void main(String[] args) {
        String fileName = "io_example.txt";

        traditionalWay(fileName);

        tryWithResources(fileName);
    }

    private static void traditionalWay(String fileName) {
        InputStream inp = null;

        try {
            // Открытие потока на чтение
            inp = new FileInputStream(fileName);

            // Проверка, сколько доступно байт для чтения.
            int bytesAvailable = inp.available();

            if (bytesAvailable > 0)
            {
                // Считывание данных побайтно в буфер.
                // На время выполнения данной операции, основной поток выполнения блокируется.
                byte[] data = new byte[bytesAvailable];
                inp.read(data);

                String str = new String(data);  // Преобразование байтового буфера в строку.
                System.out.println(str);
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                // Вызов метода close выбрасывает исключение, которое нужно обработать.
                inp.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Альтернатива: использование конструкции try-with-resources вместо
    // явного закрытия потоков (только с Java 7 и выше) - не надо вызывать метод close.
    private static void tryWithResources(String fileName) {
        try (InputStream inp = new FileInputStream(fileName))
        {
            int bytesAvailable = inp.available();

            if (bytesAvailable > 0)
            {
                byte[] data = new byte[bytesAvailable];
                inp.read(data);

                String str = new String(data);
                System.out.println(str);
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
