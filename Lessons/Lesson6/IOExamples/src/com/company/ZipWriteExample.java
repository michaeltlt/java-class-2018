package com.company;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * Пример создания zip-архива с помощью потоков.
 */
public class ZipWriteExample {

    public static void main(String[] args) {
        try {
            // Открываем поток, сжимающий поступающие данные.
            // ZipOutputStream - декоратор над потоком, работающим непосредственно с файлом.
            ZipOutputStream zout = new ZipOutputStream(
                    new FileOutputStream("zipout.zip"));

            String fileName = "hello.txt";

            // Создаем запись в архиве, ассоциированную с именем помещаемого в архив файла.
            ZipEntry ze = new ZipEntry(fileName);
            zout.putNextEntry(ze);

            // Открываем файл, который хотим заархивировать.
            File file = new File(fileName);

            BufferedInputStream in = new BufferedInputStream(
                    new FileInputStream(file));

            int length;
            byte[] buffer = new byte[1024];

            // Читаем данные из файла и записываем их в поток, связанный с архивом.
            while((length = in.read(buffer)) > 0) {
                zout.write(buffer, 0, length);
            }

            // Не забываем закрывать все потоки.
            in.close();
            zout.closeEntry();
            zout.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
