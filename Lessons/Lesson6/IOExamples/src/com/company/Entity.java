package com.company;

import java.io.Serializable;

/**
 * Классы объектов, которые предполагается сериализовать (например, сохранять в файл или передавать по сети),
 * должны ОБЯЗАТЕЛЬНО реализовывать маркерный интерфейс Serializable.
 */
class Entity implements Serializable {
    private int x;
    private int y;
    private String name;
    private transient String password;  // Это поле сохраняться (сериализоваться) не будет

    public Entity(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public String getName() {
        return this.name;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    @Override
    public String toString() {
        return "Entity {" +
                "name='" + name + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
