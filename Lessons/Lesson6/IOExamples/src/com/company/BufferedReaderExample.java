package com.company;

import java.io.*;

/**
 * Пример чтения из файла и записи в файл с использованием буферизации.
 */
public class BufferedReaderExample {

    public static void main(String[] args) {
        String fileName = "buffered_reader_example.txt";

        //Строка, которая будет записана в файл
        String data = "Мама мыла раму\n";

        // Запись строки в файл
        write(fileName, data);

        // Чтение из файла
        read(fileName);
    }


    /** Подготовка файла с данными. Для ускорения записи используется декоратор
     * BufferedWriter, которому нужно передать в конструктор ссылку на поток,
     * непосредственно работающий с файлом - FileWriter
     * @param fileName - имя файла
     * @param data - строка, которая должна быть записана в файл
     */
    private static void write(String fileName, String data) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            System.out.println("Запись в: " + fileName);

            // Несколько раз записать строку
            for (int i = 0; i < 10; i++) {
                bw.write(data);
            }

            // И еще одну строку
            bw.write("Надоело мыть раму");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Буферизированное чтение из файла. Как и в предыдущем методе, используется декоратор
     * над потоком, работающим с файлом.
     * @param fileName - имя файла
     */
    private static void read(String fileName) {
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String s = null;
            int count = 0;  // Счетчик прочитанных строк

            System.out.println("Чтение из: " + fileName);

            // Считывать данные и вывести их на экран
            while((s = br.readLine()) != null) {
                System.out.println("строка " + ++count + " прочитано:" + s);
            }

        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
