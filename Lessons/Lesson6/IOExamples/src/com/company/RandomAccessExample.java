package com.company;

import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessExample {

    public static void main(String[] args) {
        try {
            // создание нового файла (чтение и запись)
            RandomAccessFile raf = new RandomAccessFile("io_example.txt", "rw");

            // Запись в файл
            raf.writeUTF("Привет Мир!");

            // Переход в начало файла (позиция 0)
            raf.seek(0);

            // Чтение первого байта и вывод его на экран
            System.out.println("" + raf.read());
            //System.out.println("" + raf.readUTF());

            // Переход к позиции 4
            raf.seek(4);

            // Чтение байта и вывод его на экран
            System.out.println("" + raf.read());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
