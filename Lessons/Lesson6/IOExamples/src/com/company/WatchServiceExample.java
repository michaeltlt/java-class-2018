package com.company;

import java.io.IOException;
import java.nio.file.*;

/**
 * Пример отслеживания состояния текущего каталога.
 */
public class WatchServiceExample {

    public static void main(String[] args) throws IOException, InterruptedException {
        Path path = Paths.get("./");

        // WatchService следит за зарегистрированным в нем объектом.
        WatchService watcher = path.getFileSystem().newWatchService();

        // Добавляем события, которые будут отслеживаться: Создание, Модификация и Удаление файла
        path.register(watcher,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.ENTRY_DELETE);

        while(true)
        {
            // Каждый объект, за которым следит WatchService представлен в виде объекта класса WatchKey.
            WatchKey watchKey = watcher.take();

            // Извлекаем события, произошедшие с нашим объектом.
            for (WatchEvent event : watchKey.pollEvents()) {
                System.out.println(event.kind() + " : " + event.context());
            }

            // Для получения новых уведомлений о событиях, надо выполнить сброс объекта WatchKey.
            watchKey.reset();
        }
    }
}
