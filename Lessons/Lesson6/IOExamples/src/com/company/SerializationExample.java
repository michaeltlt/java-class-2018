package com.company;

import java.io.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Пример сохранения объектов в файл.
 * В данном примере сохраняется коллекция объектов вместе с самими объектами.
 */
public class SerializationExample {

    public static void main(String[] args) {
        String fileName = "serialized_objects.txt";
        List<Entity> entities = new ArrayList<>();

        // Создаем объекты, которые нужно сохранить.
        Entity ship = new Entity("Ship", 100, 20);
        Entity enemy = new Entity("Enemy", 140, 500);

        // Помещаем эти объекты в коллекцию.
        entities.add(ship);
        entities.add(enemy);

        System.out.println("Serializing");
        System.out.println(entities);
        // Сохраняем всю коллекцию вместе с объектами в файл.
        serialize(entities, fileName);

        // Восстанавливаем коллекцию со всем ее содержимым из файла.
        List<Entity> recovered = deserialize(fileName);

        System.out.println("Deserializing");
        System.out.println(recovered);
    }


    private static void serialize(List<Entity> entities, String fileName) {
        try {
            // Объект типа FileOutputStream работает с файлами.
            FileOutputStream fos = new FileOutputStream(fileName);
            // Декоратор ObjectOutputStream работает только с объектами и выполняет их сериализацию,
            // но ничего не знает о том, куда надо передать сериализованные данные.
            // Поэтому в конструктор этого объекта мы должны передать ссылку на поток, умеющий сохранять эти данные.
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(entities);  // Запись объекта в файл.
            oos.close();
        } catch(FileNotFoundException ex) {
            System.out.println("Файл не найден");
        } catch(IOException ex) {
            System.out.println("Ошибка записи в файл");
        }
    }

    private static List<Entity> deserialize(String fileName) {
        List<Entity> entities = new ArrayList<>();

        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);

            // Метод readObject восстанавливает сохраненный объект, но возвращает его как Object.
            // Поэтому необходимо его привести к исходному типу.
            entities = (List<Entity>)ois.readObject();
            ois.close();
        } catch(ClassNotFoundException ex) {
            System.out.println("Класс не найден");
        } catch(FileNotFoundException ex) {
            System.out.println("Файл не найден");
        } catch(IOException ex) {
            System.out.println("Ошибка записи в файл");
        }

        return entities;
    }
}



