package com.company;

import java.io.*;

/**
 * Сравнение чтения из файла с использованием буферизации и без.
 */
public class BufferedInput {

    public static void main(String[] args) {
        String fileName = "buffered_input_example.txt";

        // Подготовим файл с данными
        writeFile(fileName);

        // Прочитаем файл обычным способом
        regularRead(fileName);

        // Теперь прочитаем файл с применением буферизации
        bufferedRead(fileName);
    }

    // Подготовка файла - заполнение его числами.
    private static void writeFile(String fileName) {
        try (BufferedOutputStream bufOutStream = new BufferedOutputStream(new FileOutputStream(fileName))) {

            long timeStart = System.currentTimeMillis();

            for (int i = 1_000_000; i >= 0; i--) {
                bufOutStream.write(i);
            }

            // Вычисление времени, понадобившегося на запись в файл.
            long estimatedTime = System.currentTimeMillis() - timeStart;

            System.out.println("Запись: " + estimatedTime + " мс");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void regularRead(String fileName) {
        try(FileInputStream inStream = new FileInputStream(fileName)) {
            // Определить время считывания без буферизации
            long timeStart = System.currentTimeMillis();

            // Читаем данные из файла.
            while (inStream.read() != -1) {
            }

            // Вычисление времени, понадобившегося на чтение из файла.
            long estimatedTime = System.currentTimeMillis() - timeStart;

            System.out.println("Прямое чтение: " + (estimatedTime) + " мс");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void bufferedRead(String fileName) {
        try(BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(fileName))) {

            long timeStart = System.currentTimeMillis();

            while(inStream.read()!= -1) {
            }

            // Вычисление времени, понадобившегося на чтение из файла.
            long estimatedTime = System.currentTimeMillis() - timeStart;

            System.out.println("Буферизированное чтение: " + (estimatedTime) + " мс");
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
