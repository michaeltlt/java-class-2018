package com.company;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Пример простой записи строки в файл.
 */
public class PrintWriterExample {

    public static void main(String[] args) {
        String fileName = "print_writer_example.txt";

        // PrintWriter - декоратор, с помощью которого можно легко передавать
        // строки для записи другим потокам, например, потоку FileOutputStream
        // для записи в файл.
        try(PrintWriter writer = new PrintWriter(new FileOutputStream(fileName))) {
            writer.println("Hello!");
            writer.flush();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }
}
