package com.company;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileExample {
    public static void main(String[] args) {
        File file = new File("io_example.txt"); // Объект представляет файл

        File anotherFile = new File("./", "test2.txt");


        try {
            anotherFile.createNewFile();
            boolean isFileCreated = file.createNewFile(); // Создание нового пустого файла

            System.out.println(isFileCreated);
            System.out.println(file.exists());
            System.out.println(file.getFreeSpace());

            anotherFile.deleteOnExit(); // Файл будет удален при завершении работы виртуальной машины
            System.out.println("Is " + anotherFile.getName() + " exist: " + anotherFile.exists());

            boolean isDeleted = file.delete(); // Файл будет удален немедленно
            System.out.println(isDeleted);

            System.out.println("Is " + file.getName() + " exist : " + file.exists());

            File currentFolder = new File("./"); // Объект представляет директорию

            // Получить список файлов в каталоге
            System.out.println("\nСодержимое каталога " + currentFolder.getName() + ":");

            for(String fileName : currentFolder.list()) {
                System.out.println(fileName);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
