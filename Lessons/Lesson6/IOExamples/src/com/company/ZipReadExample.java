package com.company;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipReadExample {

    public static void main(String[] args) {
        try {
            ZipInputStream zin = new ZipInputStream(
                    new FileInputStream("test.zip"));
            ZipEntry entry;

            BufferedReader in = new BufferedReader(new InputStreamReader(zin));
            String str;

            while((entry = zin.getNextEntry()) != null) {
                while((str = in.readLine()) != null) {
                    System.out.println(str);
                }
            }
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
