package com.company;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

// readedInt=255. Однако
// (byte)readedInt даст значение -1

public class ByteArrayInput {

    public static void main(String[] args) {
        byte[] bytes = {1, -1, 0};
        ByteArrayInputStream in = new ByteArrayInputStream(bytes);

        int readedInt = in.read();
        System.out.println("1-й элемент: " + readedInt);
        readedInt = in.read();
        System.out.println("2-й элемент: " + readedInt);
        readedInt = in.read();
        System.out.println("3-й элемент: " + readedInt);


        byte[] outBytes = new byte[10];
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(10);
        out.write(11);
        outBytes = out.toByteArray();
        System.out.println("1-й элемент: " + outBytes[0]);
    }

}
