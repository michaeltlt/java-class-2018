package app;

/**
 * Применение паттерна "Декоратор"
 */
public class Main {

    public static void main(String[] args) {
        GenericText genText = new GenericText("This is a simple line");
        System.out.println(genText.make());

        // Декорируем строку - получаем строку с символом ';' в конце.
        SemicolonText semiText = new SemicolonText(genText);
        System.out.println(semiText.make());

        // Декорируем строку - получаем строку, обрамленную тегом <strong>
        HtmlBoldText boldText = new HtmlBoldText(genText);
        System.out.println(boldText.make());

        // Декорируем строку - получаем строку с символом ';' в конце, обрамленную тегом <strong>
        HtmlBoldText boldSemiText = new HtmlBoldText(new SemicolonText(genText));
        System.out.println(boldSemiText.make());
    }
}


interface Text {
    String make();
}

/**
 * Класс, реализующий базовый функционал: возвращает строку
 */
class GenericText implements Text {
    private String line;

    public GenericText(String line) {
        this.line = line;
    }

    @Override
    public String make() {
        return line;
    }
}


/**
 * Класс-декоратор, преобразовывающий данные декорируемого объекта в строку с точкой с запятой.
 */
class SemicolonText implements Text {
    private Text text;

    public SemicolonText(Text text) {
        this.text = text;
    }

    @Override
    public String make() {
        return text.make() + ';';
    }
}


/**
 * Класс-декоратор, преобразовывающий данные декорируемого объекта в строку, обрамленную html-тегами.
 */
class HtmlBoldText implements Text {

    private Text text;

    public HtmlBoldText(Text text) {
        this.text = text;
    }

    @Override
    public String make() {
        return "<strong>" + text.make() + "</strong>";
    }
}