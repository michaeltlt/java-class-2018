import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * Пример утечки памяти.
 * Оригинал смотрите в книге "Java. Эффективное программирование" Джошуа Блоха. Статья 6.
 * Если стек растет, а затем уменьшается, то объекты, вытолкнутые из стека, не могут быть удалены.
 */
public class Main {

    public static void main(String[] args) {
        Stack stack = new Stack();

        stack.push(new Integer(5));
        stack.push(new Integer(10));
        stack.push(new String("Hello!"));

        Object obj1 = stack.pop();
    }
}


class Stack {
    private static final int INITIAL_CAPACITY = 16;

    private Object[] elements;
    private int size;

    public Stack() {
        elements = new Object[INITIAL_CAPACITY];
    }

    public void push(Object e) {
        ensureCapacity();
        elements[size++] = e;
    }

    public Object pop() {
        if(size == 0) {
            throw new EmptyStackException();
        }

        return elements[--size];

        // Решение проблемы с утечкой:
        // дополнительно к возврату элемента из стека, необходимо удалять ссылку на этот элемент.
//        Object result = elements[--size];
//        elements[size] = null;              // Убираем устаревшую ссылку
//        return result;
    }

    /**
     * Убедиться, что в стеке есть место еще для одного элемента.
     * Иначе увеличить размер массива вдвое.
     */
    public void ensureCapacity() {
        if(elements.length == size) {
            elements = Arrays.copyOf(elements, 2 * size + 1);
        }
    }
}