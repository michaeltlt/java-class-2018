package app;

public class Main {

    public static void main(String[] args) {
        Employee worker = new Employee(30);

        Boss boss = new Boss(40);
        boss.yell();

        Employee megaBoss = new Boss(50);
        ((Boss)megaBoss).yell();
    }
}


class Person {
    // В статическом поле можно обращаться только к статическим методам.
    public static int head = setHead(1);

    public static int setHead(int h) {
        return 1;
    }
}

class Employee {
    private int age;

    public Employee(int age) {
        this.age = age;
    }


//    public Employee() {
//    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age >= 0) {
            this.age = age;
        }
    }
}

class Boss extends Employee {
    public Boss(int age) {
        // Так как в классе Employee нет конструктора по-умолчанию и конструктора без параметров,
        // требуется явный вызов конструктора с помощью ключевого слова super.
        super(age);
    }

    public void yell() {
        System.out.println("Босс кричит");
    }
}


// Демонстрация применения декомпозиции вместо наследования.
// Если в классе Automobile есть метод beep и метод с такой же функциональностью требуется в классе Printer,
// то не нужно наследовать Printer от Automobile, а вместо этого следует вынести нужный функционал
// в отдельную сущность - класс Beeper.
class Automobile {
    Beeper beeper = new Beeper();

    public void beep() {
        System.out.println("Автомобиль бип!");
        beeper.beep();
    }
}

class Printer {
    Beeper beeper = new Beeper();

    public void beep() {
        System.out.println("Принтер бип!");
        beeper.beep();
    }
}

class Beeper {
    public void beep() {
        System.out.println("Бип!");
    }
}