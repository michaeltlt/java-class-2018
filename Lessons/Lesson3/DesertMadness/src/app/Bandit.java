package app;


public class Bandit {
    private String name;
    private int power;

    public Bandit(String name, int power) {
        this.name = name;
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }
}
