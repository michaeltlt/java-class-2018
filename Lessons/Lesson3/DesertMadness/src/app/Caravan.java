package app;

import java.util.Random;

public class Caravan {
    private static final int GOLD_MAX_VALUE = 100;
    private static final int FOOD_MAX_VALUE = 100;
    private static final int POWER_MAX_VALUE = 500;

    private int gold;
    private int food;
    private int power;

    public Caravan() {
        Random random = new Random();

        // Каждый новый караван создается со случайным набором ресурсов.
        gold = random.nextInt(GOLD_MAX_VALUE);
        food = random.nextInt(FOOD_MAX_VALUE);
        power = random.nextInt(POWER_MAX_VALUE);
    }

    public int getGold() {
        return gold;
    }

    public int getFood() {
        return food;
    }

    public int getPower() {
        return power;
    }
}
