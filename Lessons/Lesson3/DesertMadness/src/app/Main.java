package app;

import java.util.Random;
import java.util.Scanner;

/**
 * Игра "Грабим корованы на Java"
 * Демонстрация объектно-ориентированного подхода: программа строится как набор сущностей (объектов),
 * которые взаимодействуют друг с другом - вызывает друг у друга методы.
 * Как правило, вызов метода меняет состояние объекта (другими словами - изменяются значения его полей).
 */
public class Main {
    private static final int CARAVAN_CHANCE = 10;   // Максимальная вероятность появления каравана

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        // Признак завершения игры. Когда значение изменится на false - нужно завершить главный игровой цикл.
        boolean isRunning = true;

        // Создаем объект, представляющий банду. Пока в нее не входит ни одного бандита, это мы сделаем позже.
        Band band = new Band();

        // Создание бандитов. Используем конструктор с двумя параметрами.
        Bandit billy = new Bandit("Билли", 10);
        Bandit joe = new Bandit("Хромой Джо", 5);
        Bandit hiew = new Bandit("Опасный Хью", 15);

        // Собираем банду (подробности - в описании метода recruit).
        band.recruit(billy);    // Строку можно прочитать как: банда нанимает billy
        band.recruit(joe);
        band.recruit(hiew);

        // Счетчик дней. Предполагалось, что с каждым днем запасы еды в банде будут убывать.
        // Такой функционал пока не реализован.
        int day = 1;

        // Игровой цикл. Условием завершения является ввод пользователем команды 'q'.
        while(isRunning) {
            System.out.println("День " + day++);

            // Расчитываем вероятность появления каравана - получаем случайное число от 0 до значения CARAVAN_CHANCE
            if(random.nextInt(CARAVAN_CHANCE) < 3) {    // Антипаттерн 'magic number'. Плохой код - неясно, что такое '3' и почему именно так.
                Caravan caravan = new Caravan();

                System.out.println("Вдали появился караван. Его сопровождают охранники (сила: " +
                        caravan.getPower() + ")");
                System.out.println("Ограбить? (y/n)");

                String command = scanner.nextLine();

                if(command.equals("y")) {
                    band.rob(caravan);  // Грабим караван. Данную запись можно прочитать как: банда грабит караван.
                }
                else if(command.equals("n")){
                    System.out.println("Вы упустили свой шанс");
                }
                else if(command.equals("q")) {
                    System.out.println("Наконец-то вы решили заняться чем-то другим.");
                    System.out.println("Попробуйте стать программистом на Java.");
                    isRunning = false;
                }
                else {
                    System.out.println("Неизвестная команда.");
                }
            }
        }
    }
}
