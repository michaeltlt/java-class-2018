package app;

public class Janitor extends Employee {
    public Janitor(String name) {
        super(name, Position.JANITOR);
    }

    public void cleanUp() {
        System.out.println(getName() + " убирается.");
    }

    public void cleanUp(Employee employee) {
        System.out.println(getName() + " убирает за " + employee.getName());
    }
}
