package app;

import java.util.Arrays;

public class Company {
    private Employee[] employees;

    public Company(int capacity) {
        employees = new Employee[capacity];
    }

    public void hire(Employee employee, double salary) {
        for (int i = 0; i < employees.length; i++) {
            if(employees[i] == null) {
                employees[i] = employee;
                employee.setSalary(salary);
                break;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        // Закомментированные строки делают то же, что цикл for после них,
        // но в функциональном стиле (начиная с Java 8 и выше)
//        Arrays.stream(employees).filter(employee -> employee != null)
//                .forEach(employee -> result.append(employee).append('\n'));
//
        for(Employee employee : employees) {
            if(employee != null) {
                result.append(employee).append('\n');
            }
        }

        return result.length() > 0 ? "Сотрудники:\n" + result.toString() : "Сотрудников нет";
    }

    public void printSalaryReport() {
        System.out.println("ОТЧЕТ О ЗАРПЛАТЕ СОТРУДНИКОВ");

        for(Employee employee : employees) {
            System.out.println(employee);
        }
    }
}
