package app;

public class Boss extends Employee {
    public Boss(String name) {
        super(name, Position.MANAGER);
    }

    public void manage() {
        System.out.println(getName() + " управляет компанией");
    }

    public void yell() {
        System.out.println(getName() + " кричит на сотрудников");
    }

    public void yell(Employee employee) {
        System.out.println(getName() + " кричит на " + employee.getName());
    }
}
