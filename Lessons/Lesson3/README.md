### Проект Example3

*Темы*
- Доступ из статического поля к методу;
- Явный вызов конструктора родительского класса с помощью super;
- Применение композиции вместо наследования.


### Проект EnumExample

*Темы*
- Использование перечислимых типов.

Дополнительно: [Перечисления в Java. Преимущества использования Enum](https://javadevblog.com/perechisleniya-v-java-preimushhestva-ispolzovaniya-enum.html)


### Проект DesertMadness
Игра "Грабим корованы на Java"

*Темы*
- Объектно-ориентированный подход к построению программ;
- Агрегация сущностей (объектов).

![Диаграмма классов проекта](https://gitlab.com/michaeltlt/java-class-2018/raw/master/Lessons/Lesson3/DesertMadness/diagram.jpg)

(проекты выполнены в среде Intellij IDEA)