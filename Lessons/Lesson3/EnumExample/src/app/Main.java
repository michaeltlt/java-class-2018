package app;

/**
 * Пример использования перечислимых типов (enums)
 */
public class Main {

    public static void main(String[] args) {
        // Используйте перечислимые типы там, где нужно ограничить диапазон допустимых значений.
        // Если бы вместо типа Sex в конструктор Person передавалась строка, например,
        // "male" вместо Sex.MALE, то такой код был бы подвержен ошибкам - мы вполне могли бы передать недопустимое
        // значение "abcdef" вместо требуемого "male".
        Person p1 = new Person("Vasya", 30, Sex.MALE);
        Person p2 = new Person("Natasha", 25, Sex.FEMALE);

        System.out.println(Sex.FEMALE.ordinal());

        System.out.println(Planet.EARTH.getWeight());
    }
}

class Person {
    public String name;
    public int age;
    public Sex sex;

    public Person(String name, int age, Sex sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
}

// Объявление перечислимого типа.
enum Sex {
    MALE,   // Значения перечисляются через запятую.
    FEMALE  // После последнего значения точка с запятой не нужна.
}

enum Planet {
    EARTH("Земля", 198213913280918L),
    MARS("Марс", 34252134142L);     // Здесь нужно поставить точку с запятой, так как далее идут другие операторы.

    private String name;
    private long weight;

    Planet(String name, long weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public long getWeight() {
        return weight;
    }
}